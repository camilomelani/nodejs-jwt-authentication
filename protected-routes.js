var express = require('express'),
    jwt     = require('express-jwt'),
    config  = require('./config'),
    dbconnection  = require('./dbconnection');




var app = module.exports = express.Router();

// Validate access_token
var jwtCheck = jwt({
  secret: config.secret,
  audience: config.audience,
  issuer: config.issuer
});

// Check for scope
function requireScope(scope) {
  return function (req, res, next) {

    var has_scopes = req.user.scope === scope;
    if (!has_scopes) {
        res.sendStatus(401);
        return;
    }
    next();
  };
}

app.use('/api/protected', jwtCheck, requireScope('full_access'));

app.get('/api/protected/movimientos/', function(req, res) {
  var query = '&q={"cliente":'+ req.query['idCliente'] +',"idCuenta":'+req.query['idCuenta']+'}';
  dbconnection.clienteMLab.get('collections/movimientos?'+dbconnection.apiKey+query, function(err, resM, body) {

   if(err) {
    } else {
      if(body.length>0){
        console.log(body[0].listado)
        res.status(200).send(body[0].listado);
      }else{
        res.status(200).send();
      }
    }
  })
})


app.get('/api/protected/cuentas/', function(req, res) {
  var query = '&q={"cliente":'+ req.query['idCliente'] +'}'+'&f={idCuenta:1}';
  dbconnection.clienteMLab.get('collections/movimientos?'+dbconnection.apiKey+query, function(err, resM, body) {

   if(err) {
    } else {
      if(body.length>0){
        console.log(body)
        res.status(200).send(body);
      }else{
        res.status(200).send();
      }
    }
  })
})

app.get('/api/protected/users/', function(req, res) {
  var query = '&f={idCliente:1,username:1}';
  dbconnection.clienteMLab.get('collections/usuarios?'+dbconnection.apiKey+query, function(err, resM, body) {

   if(err) {
    } else {
      if(body.length>0){
        console.log(body)
        res.status(200).send(body);
      }else{
        res.status(200).send();
      }
    }
  })
})



function getTransScheme(req) {
  return {
    idCliente: req.body.idCliente,
    cuentaOrigen: req.body.cuentaOrigen,
    userDSelected: req.body.userDSelected,
    cuentaD: req.body.cuentaDestino,
    importe: req.body.importe
  };
};

var numMov = 0
function getMaxidMov(cliente,cuenta,callback)
{
  var query = '&f={listado.id:1}&l=1&q={cliente:'+cliente+',idCuenta:'+cuenta+'}';
  var numMov
  var a = dbconnection.clienteMLab.get('collections/movimientos?'+dbconnection.apiKey+query,  function(err, resM, body) {
    if(err) {
      console.log(err)
    } else {
      numMov = Math.max.apply(Math, body[0].listado.map(function(o) { return o.id; }))+1
      callback(numMov);
    }
  },true)
  return numMov
}

app.put('/api/protected/trans/', function(req, res) {
  if (!req.body.idCliente || !req.body.cuentaOrigen || !req.body.userDSelected || !req.body.cuentaDestino || !req.body.importe ) {
    return res.status(400).send("You must send the source username and account, dest username and account and amount");
  }
  if (req.body.importe<0 ) {
    return res.status(400).send("amount should be positive!");
  }
  
  if ((req.body.idCliente == req.body.userDSelected) && (req.body.cuentaDestino == req.body.cuentaOrigen )) {
    return res.status(400).send("Cuenta origen debe ser distinta a cuenta destino");
  }

  var maxMovOrigen = getMaxidMov(req.body.idCliente,req.body.cuentaOrigen,function(resultObject){
          var query = '&q={cliente:'+ req.body.idCliente +',idCuenta:'+ req.body.cuentaOrigen+'}';
          var datos = {"$push": {"listado": {
                          "id": resultObject,
                          "fecha": req.body.timestamp,
                          "importe": + -req.body.importe,
                          "type": "transferencia"}}
                      }
          dbconnection.clienteMLab.put('collections/movimientos?'+dbconnection.apiKey+query, datos,
                       function(err, resM, body) {
                      });
      })

  var maxMovDestino = getMaxidMov(req.body.userDSelected,req.body.cuentaDestino,function(resultObject){
           var query = '&q={cliente:'+ req.body.userDSelected +',idCuenta:'+ req.body.cuentaDestino+'}';
           var datos = {"$push": {"listado": {
                                      "id": resultObject,
                                      "fecha": req.body.timestamp,
                                      "importe": + req.body.importe,
                                      "type": "transferencia"}}
                                  }
            dbconnection.clienteMLab.put('collections/movimientos?'+dbconnection.apiKey+query, datos,
                                   function(err, resM, body) {
                                   res.status(201).send("Transaccion exitosa");
                                  });
       })
    });
