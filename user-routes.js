var express = require('express'),
    _       = require('lodash'),
    config  = require('./config'),
    jwt     = require('jsonwebtoken');
    dbconnection  = require('./dbconnection'),
    bcrypt = require('bcrypt');

var app = module.exports = express.Router();
var BCRYPT_SALT_ROUNDS = 12;

function createIdToken(user) {
  return jwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: 60*60*5 });
}

function createAccessToken() {
  return jwt.sign({
    iss: config.issuer,
    aud: config.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    scope: 'full_access',
    sub: "lalaland|gonto",
    jti: genJti(), // unique identifier for the token
    alg: 'HS256'
  }, config.secret);
}

// Generate Unique Identifier for the access token
function genJti() {
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
      jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return jti;
}


var num = 0
class numerador {
   constructor() {
     this.inicializacion()
  }
  incrementId() {
    num=num+1;
    return num;
  };

  inicializacion(){
    var query = '&s={idCliente:-1}&l=1&f={idCliente:1}';
    var a = dbconnection.clienteMLab.get('collections/usuarios?'+dbconnection.apiKey+query,  function(err, resM, body) {
      if(err) {
        console.log(err)
      } else {
        num = body[0].idCliente
      }
    },true)
  };
};

var userNumerador = new numerador();



function getUserScheme(req) {

  var username;
  var type;
  var userSearch = {};
  console.log(req.body)
  // The POST contains a username and not an email
  if(req.body.username) {
    username = req.body.username;
    type = 'username';
    userSearch = { username: username };
  }
  // The POST contains an email and not an username
  else if(req.body.email) {
    username = req.body.email;
    type = 'email';
    userSearch = { email: username };
  }

  return {
    username: username,
    type: type,
    userSearch: userSearch
  }
}

//signup
app.post('/users', function(req, res) {

  var userScheme = getUserScheme(req);

  if (!userScheme.username || !req.body.password) {
    return res.status(400).send("You must send the username and the password");
  }

//Buscar en la base de datos el usuario
  var query = '&q={"username":"'+ userScheme.username +'"}&c=true';
  dbconnection.clienteMLab.get('collections/usuarios?'+dbconnection.apiKey+query, function(err, resM, body) {
    if(err) {
    } else {
      if(body != 0) {
        return res.status(400).send("A user with that username already exists");
      } else {
        var profile = _.pick(req.body, userScheme.type, 'password', 'extra');

      //Insertar en la base de datos el usuario
        var epass = bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS)
        req.body.idCliente = userNumerador.incrementId()
        console.log("Numerador")
        console.log(req.body.idCliente)

        bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS)
            .then(function(hashedPassword) {
              req.body.password = hashedPassword

              dbconnection.clienteMLab.post('collections/usuarios?'+dbconnection.apiKey+query, req.body,
               function(err, resM, body) {

                res.status(201).send({
                    id_token: createIdToken(profile),
                    access_token: createAccessToken(),
                    idCliente: req.body.idCliente,
                    username: req.body.idCliente,
                    nombre: req.body.nombre,
                    apellido: req.body.apellido,
                    email: req.body.email,
                    idCliente: req.body.idCliente
                  });
                });
            })
            .catch(function(error){
                next();
            });
      }
    }
  })
});

app.post('/sessions/create', function(req, res) {

  var userScheme = getUserScheme(req);

  if (!userScheme.username || !req.body.password) {
    return res.status(400).send("You must send the username and the password");
  }
  //Buscar en la base de datos el usuario

  var query = '&q={"username":"'+ userScheme.username +'"}';
  dbconnection.clienteMLab.get('collections/usuarios?'+dbconnection.apiKey+query, function(err, resM, body) {
    if(err) {
    } else {
      if (body.length==0) {
        return res.status(401).send("The username or password don't match");
      }

      if (bcrypt.compareSync(req.body.password, body[0].password)) {
        res.status(200).send({
          id_token: createIdToken(body[0].username),
          access_token: createAccessToken(),
          username: body[0].username,
          nombre: body[0].nombre,
          apellido: body[0].apellido,
          email: body[0].email,
          idCliente: body[0].idCliente
        });
      }else{
        return res.status(401).send("The username or password don't match");
      }
    }
  })
});
